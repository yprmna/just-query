create or replace trigger j
before insert on jadwal for each row 
begin
	set @batasmk = (select count(kode_mk) from jadwal where Kode_MK = new.Kode_MK);
	set @sub = (select count(hari) from jadwal where hari = new.hari and ruang = new.ruang and jam = new.jam);
	if (new.kode_mk is null or new.kode_mk = '' or @batasmk > 0) then
		SIGNAL SQLSTATE '77777' SET MESSAGE_TEXT = 'Kode MK Sudah Terjadwal'; 
	end if;
	if (@sub > 0) then
		SIGNAL SQLSTATE '77777' SET MESSAGE_TEXT = 'Jam sudah terjadwal'; 
	end if;
end;