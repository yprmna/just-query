CREATE OR REPLACE PROCEDURE sd_tambah_dosen(
		IN p_nd VARCHAR(50),
		IN p_nmk VARCHAR (50),
		IN p_hari VARCHAR (10),
		IN p_ruang VARCHAR (10),
		IN p_jam VARCHAR (12)
)
BEGIN
		INSERT INTO jadwal (Kode_MK, Nip, TA, Hari, Ruang, Jam)
		VALUES((SELECT mk.Kode_MK from matakuliah as mk where mk.Nama_MK=p_nmk),
						(SELECT d.nip from dosen as d WHERE d.Nama_Dosen=p_nd),
						'20191', p_hari, p_ruang, p_jam);
END

CALL sd_tambah_dosen('Irsyad Rizki, S.Kom.,M.Kom.', 'Manajemen Proyek', 'Selasa', 'Lab. RPL','13.00-15.00')