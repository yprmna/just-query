create or replace trigger insert_nim_krs
before insert on krs for each row
begin
	set @b = (select count(Kode_MK) from krs 
					 where nim like concat(new.nim,'%') 
					 and kode_mk = new.kode_mk);  
	
-- 	select nim from krs where nim = 
	if @b >= 1 then
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Kode MK Sudah Terjadwal'; 
	end if;
end;