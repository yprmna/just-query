-- NAMA : YOGI PERMANA
-- NIM : 1803040146

DELIMITER $$
CREATE OR REPLACE FUNCTION Nim_Baru_Dinamis(Tahun VARCHAR(4))
RETURNS VARCHAR(12)
BEGIN
			DECLARE kodebaru CHAR(12);
			DECLARE jumlah, urut, urut_baru, angkatan, angkatan_baru int;
			
			SELECT SUBSTR(Tahun,3,2) INTO angkatan;
			
			set jumlah = (SELECT COUNT(Nim)
										FROM mahasiswa 
										Where nim LIKE CONCAT(angkatan, '%'));
										
			set urut = (select substr(max(nim), 7, 3)
									from mahasiswa
									Where nim LIKE CONCAT(angkatan, '%'));
			
			IF (jumlah = 0) THEN
				BEGIN
							SET urut_baru = 1;
							SET angkatan_baru = Concat(angkatan, '0304');
							SET kodebaru = CONCAT(angkatan_baru,LPAD(urut_baru,3,'0'));
				END;
			ELSEIF (jumlah > 0) THEN
				BEGIN
							SET urut_baru = urut + 1;
							SET angkatan_baru = CONCAT(angkatan, '0304');
							SET kodebaru = CONCAT(angkatan_baru,LPAD(urut_baru,3,'0'));
				END;
			END IF;
			RETURN kodebaru;
END$$

SELECT Nim_Baru_Dinamis('2020') as 'nim baru';
