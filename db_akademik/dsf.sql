DELIMITER $$
CREATE OR REPLACE FUNCTION urut(nm varchar(25))
RETURNS varchar(12)
BEGIN
			DECLARE nourut VARCHAR(12);
			SELECT SUBSTR(nim,7,3) as no_urut from mahasiswa WHERE Nim=nm ORDER BY no_urut desc into nourut;
			RETURN nourut;
END$$
SELECT urut('Anto Kurniawan');
SELECT CONCAT("180304", LPAD(urut(Nim),4,0)) from mahasiswa;