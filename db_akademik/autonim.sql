DELIMITER$$
CREATE OR REPLACE FUNCTION nim_auto()
RETURNS VARCHAR(12)
BEGIN
			DECLARE nima VARCHAR(12);
			DECLARE tfp, urut, newnim int;
			
			SET tfp = (SELECT SUBSTR(max(nim),1,6) FROM mahasiswa);
			SET urut = (SELECT SUBSTR(max(nim),7,4) FROM mahasiswa);
			
			SET newnim = urut + 1;
			SET nima = CONCAT(tfp, LPAD(newnim, 3,'0'));
			
RETURN nima;
END$$

SELECT nim_auto();
	