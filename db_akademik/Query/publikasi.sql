CREATE TABLE `publikasi` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`nip` VARCHAR(12) NOT NUll,
	`judul` VARCHAR(255) NOT NULL,
	`jurnal_prosiding` VARCHAR(255) NOT NULL,
	`penerbit` VARCHAR(255) NOT NULL,
	`volume` VARCHAR(10) NOT NULL,
	`nomor` VARCHAR(10) NOT NULL,
	`tahun` VARCHAR(10) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`dokumen` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE = INNODB;
	
	select dokumen as dok from publikasi