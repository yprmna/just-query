DELIMITER $$
CREATE OR REPLACE PROCEDURE sp_nim_rollback()
BEGIN
					DECLARE count int;
					DECLARE count2 int;

					START TRANSACTION;
									UPDATE mahasiswa set Nim = '180304001' WHERE Nim='180304010';
									SELECT ROW_COUNT() into count;
									IF count = 0 then
										ROLLBACK;
										SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='Nim tidak ditemukan';
									ELSE
										INSERT INTO tb_log(Nim, Tabel, perubahan, waktu) VALUES ('180304001', 'mahasiswa', 'UPDATE', NOW()); 
									END IF;
									
									UPDATE krs set nim = '180304001' WHERE nim = '180304010';
									SELECT ROW_COUNT() into count2;
									IF count2 = 0 then
										ROLLBACK; 
										SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='Nim tidak ditemukan';
									ELSE 
										INSERT INTO tb_log(Nim, Tabel, perubahan, waktu) VALUES ('180304001', 'krs', 'UPDATE', NOW()); 
									END IF;
					COMMIT;
END$$

CALL sp_nim_rollback();