CREATE TABLE 	`tb_log`(
					`log_id` int(8) not null Auto_Increment,
					`Nim` VARCHAR(12) not null, 
					`Nip` VARCHAR(12) not null, 
					`Kode_MK` VARCHAR(10) not null,
					`Tabel` VARCHAR(20) not null,
					`perubahan` enum('CREATE', 'UPDATE', 'DELETE') not null,
					`waktu` TIMESTAMP not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY (`log_id`)
					)
					