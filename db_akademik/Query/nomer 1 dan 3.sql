DELIMITER $$
CREATE OR REPLACE TRIGGER J
BEFORE INSERT ON jadwal
FOR EACH ROW
		BEGIN
				DECLARE jml, jml1, jml2, jml3 int;
				SET jml = (SELECT COUNT(Kode_MK) FROM jadwal WHERE Kode_MK=NEW.Kode_MK);
				SET jml1 = (SELECT COUNT(hari) FROM jadwal WHERE Hari=NEW.Hari AND Ruang=New.Ruang AND Jam=NEW.Jam);
				SET jml2 = (SELECT COUNT(Nip) FROM jadwal WHERE Nip=New.Nip);
						IF jml > 0 THEN
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Kode MK Sudah Terjadwal';
						END IF;
						
						IF jml1 > 0 THEN
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Hari, Ruang, dan Jam Sudah Terjadwal';
						END IF;
						
						IF jml2 >= 3 THEN
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Dosen ini sudah mengampu lebih dari 3 mata kuliah';
						END IF;
		END;
$$