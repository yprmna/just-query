DELIMITER $$
CREATE OR REPLACE PROCEDURE sp_nim_rollback(
					IN nimkrs1 varchar(9),
					IN nimkrs2 VARCHAR(9),
					IN nimmhs1 VARCHAR(12),
					IN nimmhs2 VARCHAR(12)
)
BEGIN
					DECLARE count int;
					DECLARE count2 int;

					START TRANSACTION;
									UPDATE mahasiswa set Nim = nimkrs1 WHERE Nim= nimkrs2;
									SELECT ROW_COUNT() into count;
									IF count = 0 then
										ROLLBACK;
										SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='Nim tidak ditemukan';
									ELSE
										INSERT INTO tb_log(Nim, Tabel, perubahan, waktu) VALUES (nimkrs1, 'mahasiswa', 'UPDATE', NOW()); 
									END IF;
									
									UPDATE krs set nim = nimmhs1 WHERE nim = nimmhs2;
									SELECT ROW_COUNT() into count2;
									IF count2 = 0 then
										ROLLBACK; 
										SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='Nim tidak ditemukan';
									ELSE 
										INSERT INTO tb_log(Nim, Tabel, perubahan, waktu) VALUES (nimmhs1, 'krs', 'UPDATE', NOW()); 
									END IF;
					COMMIT;
END$$

CALL sp_nim_rollback('100304001', '180304001', '100304001', '180304001');