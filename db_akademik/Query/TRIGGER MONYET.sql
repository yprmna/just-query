DELIMITER $$
CREATE or REPLACE TRIGGER NIPKRS
BEFORE INSERT ON Jadwal
			FOR EACH ROW
			BEGIN
			DECLARE jml int;
			
			SET jml = (SELECT nipjdwl(NEW.Nip));
			IF jml >= 3 THEN
			BEGIN
			signal SQLSTATE '45000' set message_text = 'Dosen ini sudah mengampu lebih dari 3 mata kuliah';
			END;
			END if;
			END;
$$