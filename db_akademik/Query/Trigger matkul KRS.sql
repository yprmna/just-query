DELIMITER $$
CREATE OR REPLACE TRIGGER K
			BEFORE INSERT ON krs
			FOR EACH ROW
			BEGIN
					SET @jml = (SELECT Count(Nim) FROM krs WHERE Nim=NEW.Nim AND Kode_MK=NEW.Kode_MK);/*menghitung nilai nim yang ada pada krs yang dimana New.Nim dan New.Kode_MK sudah ada atau belum*/
					IF @jml >= 1 THEN /*jika hasil select pada variabel @jml sudah bernilai 1 maka penambahan data nim dan matakuliah pada krs
tidak dilanjutkan dan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 maka proses insert dijalankan*/
					signal SQLSTATE '45000' set message_text = 'Mata kuliah ini sudah diambil';
					end if;
			END;
$$