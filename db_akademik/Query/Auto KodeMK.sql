-- NAMA : YOGI PERMANA
-- NIM : 1803040146
-- saya menambah panjang data VARCHAR untuk kode_MK menjadi 11 supaya sesuai dengan permintaan soal
DELIMITER $$
CREATE OR REPLACE FUNCTION Auto_KodeMK(Semester INT)
RETURNS VARCHAR(12)
BEGIN
			DECLARE kodebaru CHAR(12);
			DECLARE smstr CHAR(2);
			DECLARE jumlah, urut, urut_baru, kurikulum, mk_baru int;
			
			set kurikulum = "16";
			IF (Semester = 1) THEN
				BEGIN
					SET smstr = "01";
				END;
			ELSE IF (Semester = 2) THEN
				BEGIN
					SET smstr = "02";
				END;
			END IF;
			set jumlah = (SELECT COUNT(Kode_MK)
										FROM matakuliah 
										Where Kode_MK LIKE CONCAT(kurikulum, '%'));
										
			set urut = (select substr(max(Kode_MK), 9, 3)
									from matakuliah
									Where Kode_MK LIKE CONCAT(kurikulum, '%'));		
			IF (jumlah = 0) THEN
				BEGIN
							SET urut_baru = 1;
							SET mk_baru = Concat(kurikulum, '0304', smstr);
							SET kodebaru = CONCAT(mk_baru,LPAD(urut_baru,3,'0'));
				END;
			ELSEIF (jumlah > 0) THEN
				BEGIN
							SET urut_baru = urut + 1;
							SET mk_baru = CONCAT(kurikulum, '0304', smstr);
							SET kodebaru = CONCAT(mk_baru, LPAD(urut_baru,3,'0'));
				END;
			END IF;
			RETURN kodebaru;
END$$

SELECT Nim_Baru_Dinamis('2020') as 'nim baru';
