-- NAMA : YOGI PERMANA
-- NIM : 1803040146

DELIMITER $$
CREATE TRIGGER `After_Delete_NIP`
		AFTER DELETE ON dosen
		FOR EACH ROW
		BEGIN 
				SET @tipe = 'DELETE';
				DELETE FROM jadwal where (nip=OLD.nip);
				INSERT INTO tb_log (Nip, perubahan, waktu)
				VALUES (OLD.Nip, @tipe, now());
		END;
$$


