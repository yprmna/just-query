-- NAMA : YOGI PERMANA
-- NIM : 1803040146
-- saya menambah panjang data VARCHAR untuk kode_MK menjadi 11 supaya sesuai dengan permintaan soal
DELIMITER$$
CREATE OR REPLACE FUNCTION auto_kdmk(kurikulum Char(2),Semester INT)
RETURNS VARCHAR(12)
BEGIN

			IF (Semester = 1) THEN
					BEGIN
							SET @smstr = '01';
					END;
			ELSEIF (Semester = 2) THEN
					BEGIN 
							SET @smstr = '02';
					END;
			END IF;
			
			SET @jumlah = (SELECT MAX(LOCATE(@smstr,SUBSTR(Kode_MK,1,8)))
										FROM matakuliah
										WHERE Kode_MK LIKE CONCAT(kurikulum,'%'));

			SET @urut = (select SUBSTR(max(Kode_MK),9,3)
									from matakuliah Where Kode_MK LIKE CONCAT(kurikulum,'%') and Locate(@smstr,SUBSTR(Kode_MK,7,2)));

			IF (@jumlah > 0 ) THEN
						BEGIN
								SET @urut_baru = @urut + 1;
								SET @kode_baru = CONCAT(kurikulum, '0304', @smstr, LPAD(@urut_baru,3,'0'));
						END;
			ELSE
								SET @urut_baru = 1;
								SET @kode_baru = CONCAT(kurikulum, '0304', @smstr, LPAD(@urut_baru,3,'0'));
			END IF;
			RETURN @kode_baru;
END$$

SELECT auto_kdmk('18',2);