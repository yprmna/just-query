DELIMITER $$
CREATE OR REPLACE FUNCTION Nim_Baru_Dinamis(Tahun VARCHAR(4))
RETURNS VARCHAR(12)
BEGIN
			DECLARE kodebaru CHAR(12);
			DECLARE urut int;
			DECLARE urut_baru int;
			DECLARE angkatan char(2);
			DECLARE angkatan_baru int;
			DECLARE jumlah int;
			DECLARE a int;
			
			SELECT SUBSTR(Tahun,3,2) INTO angkatan;
			SELECT COUNT(Nim) Into jumlah 
			FROM mahasiswa Where Nim LIKE CONCAT('%', angkatan, '%');
			IF (jumlah = 0) THEN
			BEGIN
						SET urut_baru = 1;
						SET angkatan_baru = Concat(angkatan, '0304');
						SET kodebaru = CONCAT(angkatan_baru,LPAD(urut_baru,3,'0'));
			END;
			ELSEIF (jumlah >= 1) THEN
			BEGIN
						SELECT MAX(SUBSTR(Nim, LOCATE("0", Nim,6),3)) INTO urut
						FROM mahasiswa
						Where Nim LIKE CONCAT('%', angkatan, '%');
						Set urut_baru= urut+1;
						SET angkatan_baru = CONCAT(angkatan, '0304');
						SET kodebaru = CONCAT(angkatan_baru,LPAD(urut_baru,3,'0'));
			END;
			END IF;
			RETURN kodebaru;
END$$

SELECT Nim_Baru_Dinamis('2020');