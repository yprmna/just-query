DELIMITER $$
CREATE OR REPLACE PROCEDURE sp_kodemk_rollback()
BEGIN
				DECLARE EXIT HANDLER FOR SQLEXCEPTION
				BEGIN
									ROLLBACK;
									GET DIAGNOSTICS CONDITION 1 @SQLSTATE = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
									SET @full_error = CONCAT("ERROR", @errno, " (",@SQLSTATE,"): ", @text);
									SELECT @full_error;
				END;

				START TRANSACTION;
									INSERT INTO matakuliah (Kode_MK, Nama_MK, Sks) VALUES ('130342218', 'Etika Profesi', 2);
									INSERT INTO jadwal (Kode_MK, Nip, TA, Hari, Ruang, Jam) VALUES ('130342218', '2160577', '20191', 
									'Selasa', 'R.III.8', '09.00-12.00');
									INSERT INTO tb_log (Kode_MK, Tabel, perubahan, waktu) VALUES ('130342218', 'matakuliah', 'CREATE', NOW());
									INSERT INTO tb_log (Kode_MK, Tabel, perubahan, waktu) VALUES ('130342218', 'jadwal', 'CREATE', NOW());
				COMMIT;
END$$

CALL sp_kodemk_rollback();