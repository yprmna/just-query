DELIMITER $$
CREATE OR REPLACE FUNCTION Nim_Baru(Tahun VARCHAR(4))
RETURNS VARCHAR(12)
BEGIN
			DECLARE kodebaru CHAR(12);
			DECLARE urut int;
			DECLARE urut_baru int;
			DECLARE angkatan char(2);
			DECLARE jumlah int;
			
			SELECT SUBSTR(Tahun,3,2) INTO angkatan;
			IF(angkatan ='18') THEN
			BEGIN
						SELECT MAX(SUBSTR(Nim,LOCATE("0",Nim,6),3)) INTO urut
						FROM mahasiswa WHERE NIM Like "18%";
						
						SET urut_baru = urut+1;
						SET kodebaru = CONCAT('180304', LPAD(urut_baru,3,'0'));
						
			END;
			ELSEIF (angkatan = '80') THEN
			BEGIN
						SELECT COUNT(Nim) INTO jumlah
						FROM mahasiswa 
						WHERE NIM LIKE "80%";
						IF (jumlah = 0) THEN
						BEGIN
									SET urut_baru = 1;
									SET kodebaru = CONCAT('800304',LPAD(urut_baru,3,'0'));
						END;
						ELSEIF (jumlah > 1) THEN
						BEGIN
									SELECT MAX(SUBSTR(Nim, LOCATE("0", Nim,6),3)) INTO urut
									FROM mahasiswa
									Where Nim LIKE "18";
									SET urut_baru = urut+1;
									SET kodebaru = CONCAT('180304',LPAD(urut_baru,3,'0'));
						END;
						END IF;
					END;
					END IF;
RETURN kodebaru;
END$$
SET @urut = (select SUBSTR(max(Kode_MK),9,3)
									from matakuliah Where Kode_MK LIKE '16%' and Locate('01',SUBSTR(Kode_MK,7,2)))
								SElect	@urut + 1;
Select Nim_Baru('2080');