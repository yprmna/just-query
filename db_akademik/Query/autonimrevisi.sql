DELIMITER$$
CREATE OR REPLACE FUNCTION nim_auto()
RETURNS VARCHAR(12)
BEGIN
			DECLARE nima VARCHAR(12);
			DECLARE tfp, urut, urut_baru, angkatan, tahun, new_angkatan int;
			
			SELECT YEAR(CURDATE()) into tahun;
			SELECT SUBSTR(tahun,3,2) INTO angkatan;
			SET tfp = (SELECT COUNT(nim)
								FROM mahasiswa
								WHERE nim LIKE CONCAT(angkatan, '%'));
			SET urut = (SELECT SUBSTR(max(nim),7,4) 
									FROM mahasiswa
									WHERE nim LIKE CONCAT(angkatan, '%'));
			
			IF (tfp = 0) THEN
				BEGIN
						SET urut = 1;
						SET new_angkatan = Concat(angkatan, '0304');
						SET nima = CONCAT(new_angkatan,LPAD(urut,4,'0'));
				END;
			ELSEIF (tfp > 0) THEN
				BEGIN
						SET urut_baru = urut + 1;
						SET new_angkatan = Concat(angkatan, '0304');
						SET nima = CONCAT(new_angkatan,LPAD(urut_baru,4,'0'));
				END;
			END IF;		
RETURN nima;
END$$

SELECT nim_auto();
	