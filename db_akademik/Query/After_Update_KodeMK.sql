-- NAMA : YOGI PERMANA
-- NIM : 1803040146

DELIMITER $$
CREATE TRIGGER `After_Update_KodeMK`
		AFTER UPDATE ON matakuliah 
		FOR EACH ROW
		BEGIN 
				SET @tipe = 'UPDATE';
				UPDATE KRS set Kode_MK = NEW.Kode_MK where  Kode_MK=OLD.Kode_MK;
				UPDATE jadwal set Kode_MK = NEW.Kode_MK WHERE Kode_MK=OLD.Kode_MK;
				INSERT INTO tb_log (Kode_MK, perubahan, waktu)
				VALUES (NEW.Kode_MK, @tipe, now());
		END;
$$



