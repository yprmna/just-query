create procedure tambah_jadwal(
			IN Nama_Matkul VARCHAR(50),
			IN Nama_DSN VARCHAR(50),
			IN Tahun_Ajaran VARCHAR(5),
			IN Hari_Kuliah VARCHAR(10),
			IN Ruangan VARCHAR(10),
			IN Jam_Kuliah VARCHAR(12)
)
INSERT Into jadwal(Kode_MK, nip, TA, hari, Ruang, Jam)
WHERE matakuliah.Nama_MK=Nama_Matkul and dosen.Nama_Dosen=Nama_DSN
VALUES (Nama_Matkul, Nama_DSN,Tahun_Ajaran,Hari_Kuliah,Ruangan,Jam_Kuliah)

call tambah_jadwal('Manajemen Proyek','Irsyad Rizki, S.Kom.,M.Kom.','20191','Selasa','Lab. RPL','13.00-15.00')