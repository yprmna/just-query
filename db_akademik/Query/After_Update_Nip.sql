-- NAMA : YOGI PERMANA
-- NIM : 1803040146

DELIMITER $$
CREATE TRIGGER `After_Update_NIP`
		AFTER UPDATE ON dosen
		FOR EACH ROW
		BEGIN 
				SET @tipe = 'UPDATE';
				UPDATE jadwal set nip = NEW.nip where nip=OLD.nip;
				INSERT INTO tb_log (Nip, perubahan, waktu)
				VALUES (NEW.Nip, @tipe, now());
		END;
$$
