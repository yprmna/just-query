-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25 Feb 2020 pada 23.54
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_akademik`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `COBA` ()  BEGIN
			INSERT INTO tb_log(Nim, `status`, Tabel, perubahan, waktu) VALUES ('100304001', 
										'Nim tidak ditemukan' , 'Mahasiswa' ,'UPDATE', NOW());
			INSERT INTO tb_log(Nim, `status`, Tabel, perubahan, waktu) VALUES ('100304001', 'Nim tidak ditemukan' , 
										'krs', 'UPDATE', NOW()); 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kodemk_rollback` ()  BEGIN
				DECLARE EXIT HANDLER FOR SQLEXCEPTION
				BEGIN
									ROLLBACK;
									GET DIAGNOSTICS CONDITION 1 @SQLSTATE = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
									SET @full_error = CONCAT("ERROR", @errno, " (",@SQLSTATE,"): ", @text);
									SELECT @full_error;
				END;

				START TRANSACTION;
									INSERT INTO matakuliah (Kode_MK, Nama_MK, Sks) VALUES ('130342218', 'Etika Profesi', 2);
									INSERT INTO jadwal (Kode_MK, Nip, TA, Hari, Ruang, Jam) VALUES ('130342218', '2160577', '20191', 
									'Selasa', 'R.III.8', '09.00-12.00');
									INSERT INTO tb_log (Kode_MK, Tabel, perubahan, waktu) VALUES ('130342218', 'matakuliah', 'CREATE', NOW());
									INSERT INTO tb_log (Kode_MK, Tabel, perubahan, waktu) VALUES ('130342218', 'jadwal', 'CREATE', NOW());
				COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_nim_rollback` (IN `nimkrs1` VARCHAR(9), IN `nimkrs2` VARCHAR(9), IN `nimmhs1` VARCHAR(12), IN `nimmhs2` VARCHAR(12))  BEGIN
					DECLARE count int;
					DECLARE count2 int;

					START TRANSACTION;
									UPDATE mahasiswa set Nim = nimkrs1 WHERE Nim= nimkrs2;
									SELECT ROW_COUNT() into count;
									IF count = 0 then
										ROLLBACK;
										SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='Nim tidak ditemukan';
									ELSE
										INSERT INTO tb_log(Nim, Tabel, perubahan, waktu) VALUES (nimkrs1, 'mahasiswa', 'UPDATE', NOW()); 
									END IF;
									
									UPDATE krs set nim = nimmhs1 WHERE nim = nimmhs2;
									SELECT ROW_COUNT() into count2;
									IF count2 = 0 then
										ROLLBACK; 
										SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='Nim tidak ditemukan';
									ELSE 
										INSERT INTO tb_log(Nim, Tabel, perubahan, waktu) VALUES (nimmhs1, 'krs', 'UPDATE', NOW()); 
									END IF;
					COMMIT;
END$$

--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `Nim_Baru_Dinamis` (`Tahun` VARCHAR(4)) RETURNS VARCHAR(12) CHARSET latin1 BEGIN
			DECLARE kodebaru CHAR(12);
			DECLARE jumlah, urut, urut_baru, angkatan, angkatan_baru int;
			
			SELECT SUBSTR(Tahun,3,2) INTO angkatan;
			
			set jumlah = (SELECT COUNT(Nim)
										FROM mahasiswa 
										Where nim LIKE CONCAT(angkatan, '%'));
										
			set urut = (select substr(max(nim), 7, 3)
									from mahasiswa
									Where nim LIKE CONCAT(angkatan, '%'));
			
			IF (jumlah = 0) THEN
				BEGIN
							SET urut_baru = 1;
							SET angkatan_baru = Concat(angkatan, '0304');
							SET kodebaru = CONCAT(angkatan_baru,LPAD(urut_baru,3,'0'));
				END;
			ELSEIF (jumlah > 0) THEN
				BEGIN
							SET urut_baru = urut + 1;
							SET angkatan_baru = CONCAT(angkatan, '0304');
							SET kodebaru = CONCAT(angkatan_baru,LPAD(urut_baru,3,'0'));
				END;
			END IF;
			RETURN kodebaru;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `Nip_Baru` () RETURNS VARCHAR(12) CHARSET latin1 BEGIN
		SET @urut = (select max(nip) + 1 from dosen); /*nip terbesar akan ditambah 1 untuk nip baru*/
		RETURN @urut;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `sf_id_barang` () RETURNS VARCHAR(6) CHARSET latin1 BEGIN
		DECLARE idbrg char(6);
		DECLARE jumlah, urut, urut_baru, idbrg1 int;
		
-- 		SELECT SUBSTR(urut,3,1) idbrg1;
		set jumlah = (SELECT COUNT(id_barang) 
									FROM barang);
		IF (jumlah = 0) THEN
				BEGIN
							SET urut_baru = 1;
							SET idbrg = CONCAT('BRG',RPAD(urut_baru,3,'0'));
				END;	
		END IF;
		RETURN idbrg;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `sf_ips` (`no_mhs` VARCHAR(12)) RETURNS DOUBLE BEGIN
			declare ips double;
			SELECT sum(CASE
					WHEN KRS.NILAI = 'A' then 4 * matakuliah.SKS
					WHEN KRS.NILAI = 'B+' then 3.25 * matakuliah.SKS
					WHEN KRS.NILAI = 'B' then 3 * matakuliah.SKS
					WHEN KRS.NILAI = 'C+' then 2.25 * matakuliah.SKS
					WHEN KRS.NILAI = 'C' then 2 * matakuliah.SKS
					WHEN KRS.NILAI = 'D' then 1 * matakuliah.SKS
					ELSE 0 * matakuliah.SKS
					END)/SUM(matakuliah.SKS) as ip
					FROM krs, matakuliah
					where krs.kode_mk=matakuliah.kode_mk and krs.nim=no_mhs
					Group by krs.nim INTO ips;
RETURN ips;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `sf_jml_mhs` (`nm_mk` VARCHAR(50)) RETURNS INT(11) BEGIN
			Declare jml int;
			SELECT COUNT(*) AS jml_mhs 
			FROM krs, matakuliah 
			where krs.Kode_MK=matakuliah.Kode_MK and matakuliah.nama_mk=nm_MK
			group by krs.kode_mk into jml;
RETURN jml;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `urut` (`nomhs` VARCHAR(12)) RETURNS VARCHAR(12) CHARSET latin1 BEGIN
			DECLARE nourut VARCHAR(12);
			SELECT SUBSTR(nim,7,3) from mahasiswa WHERE Nim=nomhs into nourut;
			RETURN nourut;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `Nip` varchar(12) NOT NULL,
  `Nama_Dosen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`Nip`, `Nama_Dosen`) VALUES
('2160293', 'Dwi Aryanto, S.T., M.Kom.'),
('2160422', 'Bangun Wijayanto, S.T.,M.Cs.'),
('2160468', 'Latiful Hayat, S.T.,M.T.'),
('2160577', 'Anton Sanjaya, S.Kom.,M.Kom.'),
('2160588', 'Teguh Wahyono, S.T.,M.T.'),
('2160887', 'Khatibul Umam, S.T.,M.Kom.'),
('2160897', 'Irsyad Rizki, S.Kom.,M.Kom.'),
('2160898', 'Noven Indra, S.Kom.,M.Kom'),
('2160899', 'Jaya Suprana, S.H,M.Hum');

--
-- Trigger `dosen`
--
DELIMITER $$
CREATE TRIGGER `After_Update_NIP` AFTER UPDATE ON `dosen` FOR EACH ROW BEGIN 
				SET @tipe = 'UPDATE';
				UPDATE jadwal set nip = NEW.nip where nip=OLD.nip;
				INSERT INTO tb_log (Nip, perubahan, waktu)
				VALUES (NEW.Nip, @tipe, now());
		END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `T_Insert_Nip` BEFORE INSERT ON `dosen` FOR EACH ROW BEGIN
						DECLARE new_nip VARCHAR(12);
						SET new_nip = (SELECT Nip_Baru()); /*mengambil nip dari function Nip_Baru()*/
						SET @tipe = 'CREATE';
						
						IF (NEW.nip is NULL or NEW.nip = '') THEN /*jika nip tidak diisi ketika melakukan proses insert pada tabel dosen maka akan secara otomatis diisi oleh function tadi*/
									SET NEW.nip=new_nip; /*Nip baru akan diisi oleh nilai set dari new_nip(mengambil nilai dari FUNCTION)*/
									INSERT INTO tb_log (Nip, perubahan, waktu) /*proses insert yang dilakukan pada tabe dosen akan tercatat pada tabel tb_log*/
									Values (new_nip, @tipe, now());
						END IF;
			END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id` int(11) NOT NULL,
  `Kode_MK` varchar(10) DEFAULT NULL,
  `Nip` varchar(12) DEFAULT NULL,
  `TA` varchar(5) DEFAULT NULL,
  `Hari` varchar(10) DEFAULT NULL,
  `Ruang` varchar(10) DEFAULT NULL,
  `Jam` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id`, `Kode_MK`, `Nip`, `TA`, `Hari`, `Ruang`, `Jam`) VALUES
(1, '130342217', '2160898', '20191', 'Selasa', 'R.III.2', '09.00-10.40'),
(2, '1403040202', '2160898', '20191', 'Senin', 'Lab. RPL', '09.00-12.00'),
(3, '1403043201', '2160898', '20191', 'Senin', 'Lab. RPL', '13.00-15.00'),
(4, '130342218', '2160887', '20191', 'Rabu', 'Lab. Cerda', '09.00-12.00'),
(5, '1403043119', '2160887', '20191', 'Rabu', 'Lab. Cerda', '13.00-15.00'),
(6, '1403043122', '2160588', '20191', 'Kamis', 'Lab. RPL', '09.00-12.00'),
(7, '1403042108', '2160422', '20191', 'Kamis', 'Lab. Cerda', '09.00-12.00'),
(8, '1403042110', '2160577', '20191', 'Senin', 'R.III.8', '09.00-12.00'),
(17, '130342219', '2160577', '20191', 'Selasa', 'R.III.8', '09.00-12.00');

--
-- Trigger `jadwal`
--
DELIMITER $$
CREATE TRIGGER `J` BEFORE INSERT ON `jadwal` FOR EACH ROW BEGIN
				SET @jml = (SELECT COUNT(Kode_MK) FROM jadwal WHERE Kode_MK=NEW.Kode_MK);/*menghitung apakah kode matakuliah yang akan ditambahkan pada jadwal sudah ada atau tidak*/
				SET @jml1 = (SELECT COUNT(hari) FROM jadwal WHERE Hari=NEW.Hari AND Ruang=New.Ruang AND Jam=NEW.Jam);/*menghitung atau mengecek Hari Ruang Jam yang akan ditambahkan pada tabel jadwal sudah ada atau tidak*/
				SET @jml2 = (SELECT COUNT(Nip) FROM jadwal WHERE Nip=New.Nip); /*no 3*/ /*menghitung jumlah nip dosen pada tabel jadwal*/
						IF @jml > 0 THEN /*jika hasil select pada variabel @jml bernilai 1 maka akan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 maka kode matkul akan ditambahkan*/
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Kode MK Sudah Terjadwal';/*signal sqlstate ini adalah function untuk menampilkan notifikasi, untuk kode '45000' itu bisa dicari sendiri di internet memiliki arti apa*/
						END IF;
						
						IF @jml1 > 0 THEN/*jika hasil select pada variabel @jml1 bernilai 1 maka akan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 maka hari ruang dan jam akan ditambahkan*/
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Hari, Ruang, dan Jam Sudah Terjadwal';
						END IF;
						
						IF @jml2 >= 3 THEN /*no 3*/ /*jika hasil select pada variabel @bernilai 3 maka akan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 atau 1 atau 2 maka nip dosen akan ditambahkan*/
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Dosen ini sudah mengampu lebih dari 3 mata kuliah';
						END IF;
		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `krs`
--

CREATE TABLE `krs` (
  `id` int(11) NOT NULL,
  `NIM` varchar(9) DEFAULT NULL,
  `Kode_MK` varchar(10) DEFAULT NULL,
  `TA` varchar(5) DEFAULT NULL,
  `Nilai` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `krs`
--

INSERT INTO `krs` (`id`, `NIM`, `Kode_MK`, `TA`, `Nilai`) VALUES
(1, '100304001', '130342217', '20191', 'B'),
(2, '100304001', '1403040202', '20191', 'B+'),
(3, '100304001', '1403043201', '20191', 'C+'),
(4, '100304002', '130342217', '20191', 'A'),
(5, '100304002', '1403040202', '20191', 'B+'),
(6, '100304002', '1403043201', '20191', 'C+'),
(7, '100304010', '130342218', '20191', 'C'),
(8, '100304010', '1403043119', '20191', 'B+'),
(9, '100304010', '1403042108', '20191', 'A'),
(10, '100304005', '1403043201', '20191', 'A'),
(11, '100304005', '1403043122', '20191', 'B'),
(12, '100304005', '1403042108', '20191', 'C+'),
(13, '100304005', '1403042110', '20191', 'B+'),
(14, '100304003', '1403040202', '20191', 'C+'),
(15, '100304003', '130342218', '20191', 'B');

--
-- Trigger `krs`
--
DELIMITER $$
CREATE TRIGGER `K` BEFORE INSERT ON `krs` FOR EACH ROW BEGIN
					SET @jml = (SELECT Count(Nim) FROM krs WHERE Nim=NEW.Nim AND Kode_MK=NEW.Kode_MK);/*menghitung nilai nim yang ada pada krs yang dimana New.Nim dan New.Kode_MK sudah ada atau belum*/
					IF @jml >= 1 THEN /*jika hasil select pada variabel @jml sudah bernilai 1 maka penambahan data nim dan matakuliah pada krs
tidak dilanjutkan dan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 maka proses insert dijalankan*/
					signal SQLSTATE '45000' set message_text = 'Mata kuliah ini sudah diambil';
					end if;
			END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `Nim` varchar(12) NOT NULL,
  `Nama_Mhs` varchar(25) NOT NULL,
  `Tgl_Lahir` date NOT NULL,
  `Alamat` varchar(200) NOT NULL,
  `Jenis_Kelamin` enum('Laki-laki','Perempuan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`Nim`, `Nama_Mhs`, `Tgl_Lahir`, `Alamat`, `Jenis_Kelamin`) VALUES
('100304001', 'DIANA BUDI', '1992-02-27', 'RT. 03 RW. 04 KARANGANYAR GANDRUNGMANGU CILACAP', 'Laki-laki'),
('100304002', 'SISKA DEWI', '1992-08-06', 'RT. 01 RW. 04 MENDALA SIRAMPOG BREBES 52272', 'Perempuan'),
('100304003', 'GALIH IMAM SAPUTRO', '1991-05-03', 'RT 04/08 BANTARBARANG REMBANG PURBALINGGA 53356', 'Laki-laki'),
('100304005', 'NIKEN PROBOWATI', '1992-03-06', 'JL STASIUN NOTOG NO 2 RT 06/02 NOTOG PATIKRAJA 53171', 'Perempuan'),
('100304010', 'UMU ROHMAH', '1992-06-05', 'JL. MASJID AL KAUSAR RT. 3 RW. 5 DONDONG KESUGIHAN CILACAP 53274', 'Laki-laki'),
('180304004', 'Anto Kurniawan', '1997-08-02', 'PONDOK KACANG PRIMA  NO: D1/4 RT: 06 RW: 08 DESA: ', 'Laki-laki');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matakuliah`
--

CREATE TABLE `matakuliah` (
  `Kode_MK` varchar(10) NOT NULL,
  `Nama_MK` varchar(50) NOT NULL,
  `Sks` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `matakuliah`
--

INSERT INTO `matakuliah` (`Kode_MK`, `Nama_MK`, `Sks`) VALUES
('130342217', 'Struktur Data dan Algoritma', 2),
('130342218', 'Sistem Cerdas', 3),
('130342219', 'Etika Profesi', 2),
('1403040202', 'Basis Data Lanjut', 3),
('1403040203', 'Pemrograman Web', 3),
('1403042108', 'Teori Komputasi', 2),
('1403042109', 'Metodologi Penelitian', 2),
('1403042110', 'Manajemen Proyek', 3),
('1403042111', 'Sistem Operasi', 2),
('1403043117', 'Interaksi Manusia dan Komputer', 2),
('1403043119', 'Komputasi Lunak', 3),
('1403043122', 'Kecerdasan Bisnis', 3),
('1403043201', 'Basis Data', 3);

--
-- Trigger `matakuliah`
--
DELIMITER $$
CREATE TRIGGER `After_Update_KodeMK` AFTER UPDATE ON `matakuliah` FOR EACH ROW BEGIN 
				SET @tipe = 'UPDATE';
				UPDATE KRS set Kode_MK = NEW.Kode_MK where  Kode_MK=OLD.Kode_MK;
				UPDATE jadwal set Kode_MK = NEW.Kode_MK WHERE Kode_MK=OLD.Kode_MK;
				INSERT INTO tb_log (Kode_MK, perubahan, waktu)
				VALUES (NEW.Kode_MK, @tipe, now());
		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_log`
--

CREATE TABLE `tb_log` (
  `log_id` int(8) NOT NULL,
  `Nim` varchar(12) NOT NULL,
  `Nip` varchar(12) NOT NULL,
  `Kode_MK` varchar(10) NOT NULL,
  `Tabel` varchar(20) NOT NULL,
  `perubahan` enum('CREATE','UPDATE','DELETE') NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_log`
--

INSERT INTO `tb_log` (`log_id`, `Nim`, `Nip`, `Kode_MK`, `Tabel`, `perubahan`, `waktu`) VALUES
(1, '180304014', '', '', 'mahasiswa', 'UPDATE', '2019-12-17 14:52:16'),
(2, '180304014', '', '', 'krs', 'UPDATE', '2019-12-17 14:52:16'),
(3, '', '', '130342219', 'matakuliah', 'CREATE', '2019-12-17 14:54:20'),
(4, '', '', '130342219', 'jadwal', 'CREATE', '2019-12-17 14:54:20'),
(5, '100304001', '', '', 'mahasiswa', 'UPDATE', '2019-12-17 14:58:39'),
(6, '100304001', '', '', 'krs', 'UPDATE', '2019-12-17 14:58:39'),
(7, '180304001', '', '', 'mahasiswa', 'UPDATE', '2020-01-10 11:03:39'),
(8, '180304001', '', '', 'krs', 'UPDATE', '2020-01-10 11:03:39'),
(9, '180304001', '', '', 'mahasiswa', 'UPDATE', '2020-01-10 11:04:55'),
(10, '180304001', '', '', 'krs', 'UPDATE', '2020-01-10 11:04:55'),
(11, '180304001', '', '', 'mahasiswa', 'UPDATE', '2020-01-10 15:20:15'),
(12, '180304001', '', '', 'krs', 'UPDATE', '2020-01-10 15:20:15'),
(13, '100304001', '', '', 'mahasiswa', 'UPDATE', '2020-01-10 15:21:32'),
(14, '100304001', '', '', 'krs', 'UPDATE', '2020-01-10 15:21:32'),
(15, '', '2160888', '', '', 'UPDATE', '2020-01-11 04:13:56'),
(16, '', '2160898', '', '', 'UPDATE', '2020-01-11 04:14:06'),
(17, '', '', '130342216', '', 'UPDATE', '2020-01-11 04:16:15'),
(18, '', '', '130342217', '', 'UPDATE', '2020-01-11 04:16:33');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_mhs_mk`
--
CREATE TABLE `vw_mhs_mk` (
`nim` varchar(12)
,`Nama_Mhs` varchar(25)
,`Kode_MK` varchar(10)
,`Nama_MK` varchar(50)
,`nilai` varchar(3)
,`jml_mhs` bigint(21)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `vw_mhs_mk`
--
DROP TABLE IF EXISTS `vw_mhs_mk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_mhs_mk`  AS  select `m`.`Nim` AS `nim`,`m`.`Nama_Mhs` AS `Nama_Mhs`,`k`.`Kode_MK` AS `Kode_MK`,`mk`.`Nama_MK` AS `Nama_MK`,`k`.`Nilai` AS `nilai`,count(`k`.`Kode_MK`) AS `jml_mhs` from ((`mahasiswa` `m` join `matakuliah` `mk`) join `krs` `k`) where ((`m`.`Nim` = convert(`k`.`NIM` using utf8)) and (`mk`.`Kode_MK` = convert(`k`.`Kode_MK` using utf8))) group by `k`.`NIM` having ((`jml_mhs` >= 2) and (`k`.`Nilai` between 'A' and 'C')) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`Nip`) USING BTREE;

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `krs`
--
ALTER TABLE `krs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`Nim`) USING BTREE;

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD PRIMARY KEY (`Kode_MK`) USING BTREE;

--
-- Indexes for table `tb_log`
--
ALTER TABLE `tb_log`
  ADD PRIMARY KEY (`log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `krs`
--
ALTER TABLE `krs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_log`
--
ALTER TABLE `tb_log`
  MODIFY `log_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
