-- NAMA : YOGI PERMANA
-- NIM : 1803040146

Delimiter $$
CREATE or REPLACE FUNCTION sf_ips(no_mhs varchar(12))
RETURNS double
BEGIN
			declare ips double;
			SELECT sum(CASE
					WHEN KRS.NILAI = 'A' then 4 * matakuliah.SKS
					WHEN KRS.NILAI = 'B+' then 3.25 * matakuliah.SKS
					WHEN KRS.NILAI = 'B' then 3 * matakuliah.SKS
					WHEN KRS.NILAI = 'C+' then 2.25 * matakuliah.SKS
					WHEN KRS.NILAI = 'C' then 2 * matakuliah.SKS
					WHEN KRS.NILAI = 'D' then 1 * matakuliah.SKS
					ELSE 0 * matakuliah.SKS
					END)/SUM(matakuliah.SKS) as ip
					FROM krs, matakuliah
					where krs.kode_mk=matakuliah.kode_mk and krs.nim=no_mhs
					Group by krs.nim INTO ips;
RETURN ips;
END$$

select sf_ips('100304010') as IPS;