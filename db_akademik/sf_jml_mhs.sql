-- NAMA : YOGI PERMANA
-- NIM : 1803040146

Delimiter $$
CREATE or REPLACE FUNCTION sf_jml_mhs(nm_mk varchar(50))
returns int
BEGIN
			Declare jml int;
			SELECT COUNT(*) AS jml_mhs 
			FROM krs, matakuliah 
			where krs.Kode_MK=matakuliah.Kode_MK and matakuliah.nama_mk=nm_MK
			group by krs.kode_mk into jml;
RETURN jml;
END $$

select sf_jml_mhs('Komputasi Lunak') as jml_mhs;