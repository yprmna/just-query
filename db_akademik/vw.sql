create view vw_mhs_mk
as
select m.nim, m.Nama_Mhs, k.Kode_MK, mk.Nama_MK, k.nilai, count(K.Kode_MK) as jml_mhs
from mahasiswa m, matakuliah mk, krs k
where (m.nim=k.nim) and (mk.Kode_MK=k.Kode_MK)
GROUP BY k.NIM
HAVING (jml_mhs >=2) and (k.nilai BETWEEN 'A' and 'C')

Select * from vw_mhs_mk;