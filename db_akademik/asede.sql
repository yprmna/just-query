DELIMITER $$
CREATE OR REPLACE TRIGGER J
BEFORE INSERT ON jadwal
FOR EACH ROW
		BEGIN
				SET @jml = (SELECT COUNT(Kode_MK) FROM jadwal WHERE Kode_MK=NEW.Kode_MK);/*menghitung apakah kode matakuliah yang akan ditambahkan pada jadwal sudah ada atau tidak*/
				SET @jml1 = (SELECT COUNT(hari) FROM jadwal WHERE Hari=NEW.Hari AND Ruang=New.Ruang AND Jam=NEW.Jam);/*menghitung atau mengecek Hari Ruang Jam yang akan ditambahkan pada tabel jadwal sudah ada atau tidak*/
				SET @jml2 = (SELECT COUNT(Nip) FROM jadwal WHERE Nip=New.Nip); /*no 3*/ /*menghitung jumlah nip dosen pada tabel jadwal*/
						IF @jml > 0 THEN /*jika hasil select pada variabel @jml bernilai 1 maka akan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 maka kode matkul akan ditambahkan*/
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Kode MK Sudah Terjadwal';/*signal sqlstate ini adalah function untuk menampilkan notifikasi, untuk kode '45000' itu bisa dicari sendiri di internet memiliki arti apa*/
						END IF;
						
						IF @jml1 > 0 THEN/*jika hasil select pada variabel @jml1 bernilai 1 maka akan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 maka hari ruang dan jam akan ditambahkan*/
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Hari, Ruang, dan Jam Sudah Terjadwal';
						END IF;
						
						IF @jml2 >= 3 THEN /*no 3*/ /*jika hasil select pada variabel @bernilai 3 maka akan menampilkan notifikasi dengan kode dibawah ini, jika bernilai 0 atau 1 atau 2 maka nip dosen akan ditambahkan*/
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Dosen ini sudah mengampu lebih dari 3 mata kuliah';
						END IF;
		END;
$$