-- NAMA : YOGI PERMANA
-- NIM : 1803040146

DELIMITER $$ /*Membuat function untuk penomoran urut nip dosen yang nantinya akan mengenerate nip secara otomatis*/
CREATE OR REPLACE FUNCTION Nip_Baru()	
RETURNS VARCHAR(12)
BEGIN
		SET @urut = (select max(nip) + 1 from dosen); /*nip terbesar akan ditambah 1 untuk nip baru*/
		RETURN @urut;
END$$

DELIMITER $$
CREATE TRIGGER T_Insert_Nip
			BEFORE INSERT ON dosen
			FOR EACH ROW
			BEGIN
						DECLARE new_nip VARCHAR(12);
						SET new_nip = (SELECT Nip_Baru()); /*mengambil nip dari function Nip_Baru()*/
						SET @tipe = 'CREATE';
						
						IF (NEW.nip is NULL or NEW.nip = '') THEN /*jika nip tidak diisi ketika melakukan proses insert pada tabel dosen maka akan secara otomatis diisi oleh function tadi*/
									SET NEW.nip=new_nip; /*Nip baru akan diisi oleh nilai set dari new_nip(mengambil nilai dari FUNCTION)*/
									INSERT INTO tb_log (Nip, perubahan, waktu) /*proses insert yang dilakukan pada tabe dosen akan tercatat pada tabel tb_log*/
									Values (new_nip, @tipe, now());
						END IF;
			END;
$$
							
