DELIMITER $$
CREATE OR REPLACE PROCEDURE INSERT_MHS(
		IN p_nim VARCHAR(12),
		IN p_nmhs VARCHAR (25),
		IN p_tgl Date,
		IN p_alamat VARCHAR (200),
		IN p_jk ENUM('Laki-laki', 'Perempuan')
)
BEGIN
		INSERT INTO mahasiswa(nim, Nama_Mhs, Tgl_Lahir, Alamat, Jenis_Kelamin)
		VALUES((SELECT Nim_Baru_Dinamis(p_nim)), 
						p_nmhs,
						p_tgl, 
						p_alamat, 
						p_jk);
END$$

CALL INSERT_MHS('2019', 'Garoxx', '2001-12-12' ,'Mamankk', 'Laki-laki');

Select Nim_Baru_Dinamis('2019');