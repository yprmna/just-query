create procedure tambah_pembelian(
			IN idbrg VARCHAR(6),
			IN idspl VARCHAR(6),
			IN jml SMALLINT,
			IN hrg int
)
INSERT Into pembelian(id_pembelian, id_barang, id_supplier, tanggal, jumlah, harga)
VALUES ((Select sf_id_pembelian()), idbrg, idspl, now(), jml, hrg)

CALL tambah_pembelian('BRG-01', 'SP-001', 3, 12000)