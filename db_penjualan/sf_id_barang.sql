-- Saya yang bertandatangan di bawah ini menyatakan tidak mencontek/berbuat curang selama mengerjakan soal ujian ini.
-- Jika ternyata saya melanggar, saya akan bersedia dikenakan sanksi berupa dibatalkan nilai ujian mata kuliah ini
-- 																																							Yang menyatakan,
-- 																																							
-- 																																							Yogi Permana
-- 																																							1803040146
-- 
DELIMITER$$
CREATE OR REPLACE FUNCTION sf_id_barang()
RETURNS VARCHAR(6)
BEGIN
		DECLARE idbrg char(6);
		DECLARE jumlah, urut, urut_baru int;
		
-- 		SELECT SUBSTR(urut,3,1) idbrg1;
		set jumlah = (SELECT COUNT(id_barang) 
									FROM barang);
		set urut = (select substr(max(id_barang), 6, 1)
								from barang);
		
		IF (jumlah = 0) THEN
				BEGIN
							SET urut_baru = 1;
							SET idbrg = CONCAT('BRG-',RPAD('0',2, urut_baru));
				END;
		ELSEIF (jumlah > 0) THEN
				BEGIN
							SET urut_baru = urut + 1;
							SET idbrg = CONCAT('BRG-',RPAD('0',2, urut_baru));
				END;
			END IF;
		RETURN idbrg;
END$$

SELECT sf_id_barang();
		
				
		