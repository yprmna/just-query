DELIMITER $$
CREATE TRIGGER T_Insert_Pembelian
			BEFORE INSERT ON pembelian
			FOR EACH ROW
			BEGIN
						DECLARE new_idpbl VARCHAR(11);
						SET new_idpbl = (SELECT sf_id_pembelian());
						SET @tipe = 'CREATE';
						SET @ket = 'Pembelian : id_pembelian';
						
						IF (NEW.id_pembelian is NULL or NEW.id_pembelian = '') THEN
									SET NEW.id_pembelian=new_idpbl;
									INSERT INTO tb_log (id_pembelian, Keterangan, perubahan, waktu)
									Values (new_idpbl, @ket,  @tipe, now());
						END IF;
			END;
$$

							
