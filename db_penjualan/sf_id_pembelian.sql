-- Saya yang bertandatangan di bawah ini menyatakan tidak mencontek/berbuat curang selama mengerjakan soal ujian ini.
-- Jika ternyata saya melanggar, saya akan bersedia dikenakan sanksi berupa dibatalkan nilai ujian mata kuliah ini
-- 																																							Yang menyatakan,
-- 																																							
-- 																																							Yogi Permana
-- 																																							1803040146
-- 
DELIMITER$$
CREATE OR REPLACE FUNCTION sf_id_pembelian()
RETURNS VARCHAR(11)
BEGIN
		DECLARE idpbl char(11);
		DECLARE jumlah, urut, urut_baru int;
		
-- 		SELECT SUBSTR(urut,3,1) idbrg1;
		set jumlah = (SELECT COUNT(id_pembelian) 
									FROM pembelian);
		set urut = (select substr(max(id_pembelian), 10, 1)
								from pembelian);
		
		IF (jumlah = 0) THEN
				BEGIN
							SET urut_baru = 1;
							SET idpbl = CONCAT('PB-2020',RPAD('00',3, urut_baru));
				END;
		ELSEIF (jumlah > 0) THEN
				BEGIN
							SET urut_baru = urut + 1;
							SET idpbl = CONCAT('PB-2020',RPAD('00',3, urut_baru));
				END;
			END IF;
		RETURN idpbl;
END$$

SELECT sf_id_pembelian();
